namespace :prismo do
  desc "TODO"
  task seed_comments: :environment do
    bodies = [
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend mi ut massa dictum egestas. Etiam rutrum tortor vel orci vehicula blandit. Cras placerat leo dolor, nec euismod ipsum fringilla at. Nullam auctor maximus fringilla. Integer et lorem quis elit scelerisque suscipit in in leo. Vivamus pretium elit sed ipsum laoreet, efficitur imperdiet neque facilisis. Nam finibus ante id aliquet ullamcorper. Donec eget ex nec est volutpat finibus. Cras ornare nulla vel tellus iaculis tristique. Nunc dictum vel urna quis consectetur. Donec malesuada felis quis purus bibendum maximus. Duis dignissim orci nec leo sollicitudin, at laoreet augue ultricies.',
      'Duis arcu enim, interdum at lorem vel, dignissim eleifend augue. Aliquam sed commodo quam. Integer lorem massa, aliquet id fringilla ut, elementum a ligula. Quisque nec condimentum urna. Proin sed purus eget lacus gravida bibendum. Nam ac est et felis interdum lobortis. Ut eu enim gravida, blandit justo at, tempus felis. In eget tristique justo. Etiam viverra, leo quis pharetra eleifend, est turpis auctor urna, id euismod diam velit in velit. Nulla tristique turpis eu lectus cursus, quis pharetra felis laoreet. Integer sit amet ultricies metus, non auctor ligula.',
      'In dignissim vestibulum neque, quis fringilla velit scelerisque nec. Pellentesque nec placerat massa. Nunc eu lorem tortor. Ut blandit massa ac lorem lacinia tempus. Phasellus eu hendrerit nisi. Nulla gravida sagittis feugiat. Aliquam ultricies ultrices nisi sed consequat. Mauris a arcu eget ligula hendrerit venenatis.'
    ].freeze

    100.times do |i|
      account = Account.order('RANDOM()').first
      parent = Comment.order('RANDOM()').first
      story = parent.story

      comment = parent.children.create! account: account,
                                        story: story,
                                        body: bodies.sample

      comment.cache_depth
      comment.cache_body

      puts i
    end
  end

  desc "TODO"
  task cast_empty_likes: :environment do
    puts 'Enter story ID:'
    story_id = STDIN.gets.chomp
    story = Story.find(story_id)
    puts "Story found: #{story.title}"

    puts 'Number of likes:'
    likes_count = STDIN.gets.chomp.to_i

    story.update(likes_count: story.likes_count + likes_count)

    puts "Likes counter incremented. New value: #{story.reload.likes_count}"
  end

  desc "Sets up Prismo"
  task setup: :environment do
    puts '=== Creating master group'
    Group.create!(name: 'General', supergroup: true)
    puts 'Done.'

    puts '=== Creating admin account'
    Rake::Task['prismo:create_admin'].invoke
    puts 'Done.'
  end

  desc "Create admin user"
  task create_admin: :environment do
    require 'io/console'

    puts 'Create a new admin user'
    puts

    print 'Username: '
    username = STDIN.gets.chomp

    print 'Display name: '
    displayname = STDIN.gets.chomp

    print 'E-mail: '
    email = STDIN.gets.chomp

    print 'Password: '
    password = STDIN.noecho(&:gets).chomp
    puts

    account = Account.create!(username: username, display_name: displayname)
    User.create!(
      account: account,
      email: email,
      password: password,
      is_admin: true,
      confirmed_at: Time.now
    )

    puts
    puts 'Admin user created.'
  end

  desc "Create regular user with account"
  task create_user: :environment do
    require 'io/console'

    puts 'Create a new user'
    puts

    print 'Username: '
    username = STDIN.gets.chomp

    print 'Display name: '
    displayname = STDIN.gets.chomp

    print 'E-mail: '
    email = STDIN.gets.chomp

    print 'Password: '
    password = STDIN.noecho(&:gets).chomp
    puts

    User.create!(
      email: email,
      password: password,
      confirmed_at: Time.now,
      account_attributes: {
        username: username,
        display_name: displayname
      }
    )

    puts
    puts 'User created.'
  end

  task refresh_accounts_karma: :environment do
    puts 'Refreshing accounts karma'

    Account.all.each do |account|
      account.update(
        posts_karma: account.stories.sum(:likes_count),
        comments_karma: account.comments.sum(:likes_count)
      )
      print '.'
    end

    puts
  end
end
