FactoryBot.define do
  factory :like do
    account
    association :likeable, factory: :story
  end
end
