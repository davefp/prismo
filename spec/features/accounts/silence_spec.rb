require 'rails_helper'

feature 'Silencing account' do
  let(:home_page) { Stories::ShowPage.new }
  let(:story_page) { Stories::ShowPage.new }
  let(:sign_in_page) { SignInPage.new }

  let(:user) { create(:user, :with_account, password: 'TestPass') }
  let!(:story) { create(:story) }
  let!(:comment) { create(:comment, story: story) }

  before do
    user.account.update(silenced: true)
  end

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  scenario 'silenced user wants to add sub-comment', js: true do
    sign_user_in
    story_page.load(id: story.id)

    first_comment = story_page.comments_tree.comments.first

    expect(first_comment).to_not have_reply_btn
  end

  scenario 'silenced user wants to add root comment', js: true do
    sign_user_in
    story_page.load(id: story.id)

    expect(story_page.new_comment_form).to_not have_comment_body_input
    expect(story_page).to have_content "You can't post comments because your account has been silenced."
  end

  scenario 'silenced user wants to add a story', js: true do
    sign_user_in
    home_page.load

    home_page.navbar.add_story_link.click
    expect(home_page).to have_content 'not authorized'
  end

  scenario 'silenced user wants to vote on a story', js: true do
    sign_user_in
    story_page.load(id: story.id)

    story_page.stories.first.click_like_button
    expect(story_page).to have_content 'not authorized'
  end
end
