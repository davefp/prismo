require_relative '../../sections/comment_section'

class Accounts::CommentsPage < SitePrism::Page
  set_url '/@{username}/comments'
  set_url_matcher %r{\/@\w+\/comments\z}

  sections :comments, CommentSection, '.comment'
end
