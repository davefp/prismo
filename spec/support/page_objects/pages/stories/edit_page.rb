class Stories::EditPage < SitePrism::Page
  set_url '/posts{/id}/edit'
  set_url_matcher %r{\/posts\/\d+(\/edit)?\z}

  element :url_field, 'input#story_url'
  element :title_field, 'input#story_title'
  element :tag_list_field, '.tagify__input'
  element :description_field, 'textarea#story_description'
  element :submit_button, '[type="submit"]'
end
