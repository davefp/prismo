require 'rails_helper'

describe ActivityPub::DomainBlocks::Create do
  let(:domain) { 'example.com' }
  let(:severity) { 'silence' }
  let!(:remote_account) { create(:account, domain: 'example.com') }
  let!(:other_remote_account) { create(:account, domain: 'mastodon.social') }

  before do
    allow(Accounts::SilenceJob).to receive(:perform_later).and_return(true)
    allow(Accounts::SuspendJob).to receive(:perform_later).and_return(true)
  end

  let(:inputs) do
    {
      domain: domain,
      severity: severity
    }
  end

  describe '#run' do
    let(:subject) { described_class.run!(inputs) }

    it 'creates DomainBlock record' do
      expect { subject }.to change(ActivityPub::DomainBlock, :count).by(1)
    end

    context 'when severity is silence' do
      let(:severity) { 'silence' }

      it 'silences all existing accounts for given domain' do
        subject
        expect(Accounts::SilenceJob)
          .to have_received(:perform_later)
          .with(remote_account.id)
      end
    end

    context 'when severity is suspend' do
      let(:severity) { 'suspend' }

      it 'suspends all existing accounts for given domain' do
        subject
        expect(Accounts::SuspendJob)
          .to have_received(:perform_later)
          .with(remote_account.id)
      end
    end
  end
end
