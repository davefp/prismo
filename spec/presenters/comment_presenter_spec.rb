require 'rails_helper'

describe CommentPresenter do
  let(:comment) { create(:comment) }
  let(:liked_ids) { [] }
  let(:options) do
    {
      liked_ids: liked_ids
    }
  end
  let(:presenter) { described_class.new(comment, options) }

  describe '#root_classes' do
    subject { presenter.root_classes }

    context 'when comment is liked' do
      let(:liked_ids) { [comment.id] }

      it { expect(subject).to include 'comment--liked' }
    end

    context 'when comment is removed' do
      let(:comment) { create(:comment, :removed) }

      it { expect(subject).to include 'comment--removed' }
    end

    context 'when comment author is silenced' do
      before { comment.account.update(silenced: true) }

      it { expect(subject).to include 'comment--silenced' }
    end
  end
end
