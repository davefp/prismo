class Form::AdminSettings
  include ActiveModel::Model

  delegate(
    :site_title,
    :site_title=,
    :site_description,
    :site_description=,
    :open_registrations,
    :open_registrations=,
    :closed_registrations_message,
    :closed_registrations_message=,
    :stories_per_day,
    :stories_per_day=,
    :story_likes_per_day,
    :story_likes_per_day=,
    :comment_likes_per_day,
    :comment_likes_per_day=,
    :story_title_update_time_limit,
    :story_title_update_time_limit=,
    to: Setting
  )
end
