class ActivityPub::Activity::Like < ActivityPub::Activity
  def perform
    @original_resource = story_from_uri(object_uri) || comment_from_uri(object_uri)
    return if should_be_skiped?

    case original_resource
    when Story
      Stories::Like.run! story: original_resource, account: account
    when Comment
      Comments::Like.run! comment: original_resource, account: account
    end
  end

  private

  attr_accessor :original_resource

  def should_be_skiped?
    original_resource.nil? ||
      !original_resource.account.local? ||
      delete_arrived_first?(json['id']) ||
      account.liked?(original_resource)
  end
end
