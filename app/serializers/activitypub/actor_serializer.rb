class ActivityPub::ActorSerializer < ActivityPub::BaseSerializer
  def data
    result = {
      id: account_url(object.username),
      type: 'Person',
      name: object.display_name,
      preferredUsername: object.username,
      summary: object.bio,
      inbox: activitypub_account_inbox_url(object.username),
      outbox: outbox_activitypub_account_url(object.username),
      url: account_url(object.username),
      publicKey: public_key,
    }

    result[:icon] = icon

    result
  end

  private

  def public_key
    {
      id: ActivityPub::TagManager.instance.uri_for(object) + '#main-key',
      owner: account_url(object.username),
      publicKeyPem: object.public_key
    }
  end

  def icon
    return nil if !object.avatar_data? || !object.avatar_attacher.stored?

    ActivityPub::ImageSerializer.new(object.avatar).data
  end
end
