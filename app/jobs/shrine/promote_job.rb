class Shrine::PromoteJob < ApplicationJob
  queue_as :default

  def perform(data)
    Shrine::Attacher.promote(data)

    send_notification(data)
    distribute_update(data)
  end

  private

  def send_notification(data)
    case data['shrine_class']
    when 'StoryThumbUploader'
      story = Story.find(data['record'][1])
      Stories::BroadcastChanges.run! story: story
    end
  end

  def distribute_update(data)
    case data['shrine_class']
    when 'AccountAvatarUploader'
      account = Account.find(data['record'][1])
      ActivityPub::UpdateDistributionJob.call_later(account)
    end
  end
end
