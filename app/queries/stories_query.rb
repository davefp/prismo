class StoriesQuery
  attr_reader :relation

  def initialize(relation = Story.all)
    @relation = relation
  end

  def with_includes
    relation.includes(:account, :url_meta, :tags, :taggings)
  end

  def hot
    with_includes
      .order(Arel.sql('ranking(likes_count, stories.created_at::timestamp, 3) DESC'))
      .where('stories.created_at > ?', Story::HOT_DAYS_LIMIT.days.ago)
  end

  def recent
    with_includes.order(created_at: :desc)
  end

  def for_account(account)
    with_includes.where(account_id: account.id)
  end

  def tagged_with(tags)
    with_includes.tagged_with(names: tags)
  end

  def by_followed_accounts(account)
    following_ids_sql = 'SELECT target_account_id FROM follows WHERE account_id = :account_id'
    with_includes.where("account_id IN (#{following_ids_sql})", account_id: account.id)
  end

  def without_silenced
    with_includes.where(accounts: { silenced: false })
  end
end
