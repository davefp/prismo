class TagsQuery
  attr_reader :relation

  def initialize(relation = Gutentag::Tag.all)
    @relation = relation
  end

  def all
    relation
  end

  def popular
    all.order(taggings_count: :desc)
  end
end
