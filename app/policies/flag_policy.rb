class FlagPolicy < ApplicationPolicy
  def index?
    user.present? && user.is_admin?
  end

  def new?
    user.present? && !user.silenced?
  end

  def create?
    user.present? && !user.silenced?
  end

  def edit?
    user.account == record.actor || user.is_admin?
  end

  def update?
    user.account == record.actor || user.is_admin?
  end

  def resolve?
    user.present? && user.is_admin?
  end

  def unresolve?
    user.present? && user.is_admin?
  end
end
