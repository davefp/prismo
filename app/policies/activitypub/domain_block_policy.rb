module ActivityPub
  class DomainBlockPolicy < ApplicationPolicy
    def index?
      user.present? && user.is_admin?
    end

    def new?
      user.present? && user.is_admin?
    end

    def create?
      user.present? && user.is_admin?
    end

    def destroy?
      user.present? && user.is_admin?
    end
  end
end
