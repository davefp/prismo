class FollowService
  def initialize(actor, account)
    @actor = actor
    @account = account
  end

  def call
    return if actor.following?(account) || actor.requested_follow?(account)

    if account.locked?
      request_follow!
    else
      direct_follow!
    end
  end

  private

  attr_reader :actor, :account

  def request_follow!
    follow_request = FollowRequest.create!(account: actor,
                                           target_account: account)

    if account.local?
      CreateNotificationJob.call('follow_requested', author: actor,
                                                     recipient: account,
                                                     notifable: follow_request)
    end

    follow_request
  end

  def direct_follow!
    follow = actor.follow!(account)

    if account.local?
      CreateNotificationJob.call('new_follower', author: actor,
                                                 recipient: account)
    end

    follow
  end
end
