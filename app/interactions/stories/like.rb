class Stories::Like < ActiveInteraction::Base
  object :story
  object :account

  def to_model
    ::Like.new
  end

  def execute
    return existing_like if existing_like.present?

    like = ::Like.new(likeable: story)
    like.account = account

    if like.save
      account.touch(:last_active_at)
      Accounts::UpdateKarmaJob.perform_later(story.account.id, 'Story')
      # Stories::BroadcastChanges.run! story: story
    else
      errors.merge!(like.errors)
    end

    like
  end

  private

  def existing_like
    @existing_like ||= ::Like.find_by(likeable: story, account: account)
  end
end
