class Stories::Update < Stories::CreateUpdateBase
  object :story

  validates :url, url: { allow_blank: true }
  validate :url_or_description_required
  validate :title_update_time_limit

  def execute
    story.title = title if title?
    story.tag_names = tags if tag_list?
    story.description = description if description?
    story.url = url if url.present? && can_update_url?

    story.modified_at = Time.current
    story.modified_count += 1

    if story.save
      after_story_save_hook(story)
    else
      errors.merge!(story.errors)
    end

    story
  end

  def persisted?
    true
  end

  private

  def after_story_save_hook(story)
    Stories::ScrapJob.perform_later(story.id) if story.url_changed?
    Stories::BroadcastChanges.run!(story: story)
    ActivityPub::UpdateDistributionJob.call_later(story) if story.local?
  end

  def title_update_time_limit
    return unless title_changed?

    limit = Setting.story_title_update_time_limit
    errors.add(:title, "can't be edited after #{limit} minutes") unless can_update_title?
  end

  def title_changed?
    title? && title != story.title
  end

  def can_update_title?
    StoryPolicy.new(account.user, story).update_title?
  end

  def can_update_url?
    Stories::UpdatePolicy.new(account.user, story).update_url?
  end
end
