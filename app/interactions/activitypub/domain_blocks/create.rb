module ActivityPub
  module DomainBlocks
    class Create < ActiveInteraction::Base
      string :domain
      string :severity

      interface :suspend_job, default: Accounts::SuspendJob
      interface :silence_job, default: Accounts::SilenceJob

      def execute
        @domain_block = ActivityPub::DomainBlock.new
        @domain_block.domain = domain
        @domain_block.severity = severity

        if domain_block.save
          resolve_or_suspend_accounts
        else
          errors.merge!(domain_block.errors)
        end

        domain_block
      end

      private

      attr_reader :domain_block

      def resolve_or_suspend_accounts
        accounts_for_domain.each do |account|
          case domain_block.severity
          when 'suspend'
            suspend_job.perform_later(account.id)
          when 'silence'
            silence_job.perform_later(account.id)
          end
        end
      end

      def accounts_for_domain
        ::Account.remote.where(domain: domain)
      end
    end
  end
end
