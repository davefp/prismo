class Comments::Update < ActiveInteraction::Base
  object :comment
  string :body

  def execute
    comment.body = body if body?

    comment.modified_at = Time.current
    comment.modified_count += 1

    if comment.save
      after_comment_save_hook(comment)
    else
      errors.merge!(comment.errors)
    end

    comment
  end

  def persisted?
    true
  end

  private

  def after_comment_save_hook(comment)
    comment.cache_body
    Comments::BroadcastChanges.run! comment: comment
    ActivityPub::UpdateDistributionJob.call_later(comment) if comment.local?
  end
end
