class AddDefaultValueToVotesCount < ActiveRecord::Migration[5.2]
  def change
    change_column :stories, :votes_count, :integer, default: 0
    change_column :comments, :votes_count, :integer, default: 0
  end
end
