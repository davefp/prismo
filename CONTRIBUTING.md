# Contributing to Prismo

Thank you for your interest in Prismo!

You can contribute in the following ways:
* Spreading the word about Prismo
* Join in design discussions on [SocialHub](https://socialhub.network/c/prismo)
* [Finding and reporting bugs](#bug-reports)
* [Requesting new features](#feature-requests)
* [Fixing bugs or implementing features](#workflow)
* Improving the documentation

Bug reports and feature suggestions can be submitted on the GitLab repository. Please make sure that you are not submitting duplicates, and that a similar report or request has not already been resolved or rejected in the past using the search function. Please also use descriptive, concise titles.

## Bug Reports
If you think you have found a bug, please check that it hasn't already been reported. You can do that by going to [Issues](https://gitlab.com/mbajur/prismo/issues) and searching for related keywords. After verifying an issue doesn't already exist, create a new issue and title it with a simple description of the bug. In the body of the issue, add a more complete description if necessary. Be sure to include steps to reproduce the bug. Once you file a bug, maintainers or contributors will verify that it can be reproduced and triage the issue so a developer can work on it.

## Feature Requests
If you want to request a new feature, please check that it hasn't already been requested and check closed issues to verify that it hasn't already been rejected for any reason. After verifying you have a new request, create an issue and give it a simple, clear title. Describe your request more completely in the body of the issue. Include what value you think this adds to Prismo and examples of similar features in other software.

## Workflow
Prismo development utilizes GitLab for its public repository. Bug reports and feature requests should be filed as [Issues](https://gitlab.com/mbajur/prismo/issues). Issues will be triaged by project maintainers according to the project priorities. Labels indicate the areas of the software the bug/feature is related to and it's priority. Weight indicates amount of work needed by a developer. The more weight issue has, the more work a developer need to put into the issue to resolve it.

If you are a developer and you want to work on an issue, leave a comment on the issue to say that you are working on it. If it's a large bug/feature or you are unsure of the best solution, feel free to open a WIP merge request so maintainers can guide you as you work.

## Merge requests
Please use clean, concise titles for your merge requests.

The smaller the set of changes in the merge request is, the quicker it can be reviewed and merged. Splitting tasks into multiple, smaller merge requests is often preferable.

Please make sure merge requests for new features includes test specs for the new code.